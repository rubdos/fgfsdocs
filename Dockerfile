FROM fedora:latest

RUN dnf install -y \
        texlive-collection-basic \
        texlive-collection-latex \
        texlive-tikzinclude \
        python3 python3-pip python3-sqlite \
        openssh-clients openssh && dnf clean all

ADD requirements.txt requirements.txt

RUN pip install -r requirements.txt
