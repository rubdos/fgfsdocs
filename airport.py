import requests
import datetime
import xml.etree.ElementTree as etree
from xml.etree.ElementTree import XMLParser
from subprocess import call

from jinja2 import Environment, Template, FileSystemLoader

from misc import *

svn_base = "http://ns334561.ip-5-196-65.eu:8888/trunk/data/Scenery/"
airport_base = svn_base + "Airports/"

env = Environment(
    loader=FileSystemLoader('templates'),
)
env.globals['drange'] = lambda *args: list(drange(*args))

ap_map_margin = .7
ap_map_long = 29.6 - ap_map_margin*2
ap_map_short = 21 - ap_map_margin*2
ap_map_top_margin = 2
env.globals['ap_map_margin'] = ap_map_margin  # cm
env.globals['ap_map_long'] = ap_map_long
env.globals['ap_map_short'] = ap_map_short
env.globals['ap_map_top_margin'] = ap_map_top_margin
env.globals['cos'] = cos
env.globals['sin'] = sin


def get_ICAOs(s=requests.Session()):
    response = s.get(svn_base + "Airports/index.txt", stream=True)
    for line in response.iter_lines():
        line = line.split(b"|")
        if(len(line) > 0):
            ICAO = line[0].decode()
            if len(ICAO) != 4:
                print(ICAO + " is not an ICAO code")
            yield ICAO


class Airport:

    def get_all(s=requests.Session()):
        return map(lambda x: Airport(x), get_ICAOs(s))

    def __init__(self, icao):
        self.icao = icao

    def base_url(self):
        return (airport_base +
                self.icao[0] + "/" +
                self.icao[1] + "/" +
                self.icao[2] + "/")

    def fetch_files(self, s=requests.Session()):
        print("Downloading Airportdata for " +
              self.icao + " from TerraSync servers")
        filenames = ["ils", "threshold", "twr", "groundnet"]

        files = {}
        for f in filenames:
            print("Downloading " + f + " data for " + self.icao)
            response = s.get(self.base_url() + self.icao +
                             "." + f + ".xml", stream=True)
            if response.status_code != 200:
                print(f + " data for " + self.icao +
                      " returns " + str(response.status_code))
                continue
            files[f] = etree.fromstring(response.content)

        self.ils = files['ils'] if 'ils' in files else None
        self.threshold = files['threshold'] if 'threshold' in files else None
        self.twr = files['twr'] if 'twr' in files else None
        self.groundnet = files['groundnet'] if 'groundnet' in files else None

    def build_chart(self, target):
        self.target = target
        ensure_dir(target)
        with open(target + '/' + self.icao + ".tex", "w") as tex:
            tex.write(env.get_template('airport.tex.j2')
                      .render())

    def prepare_taxiways(self):
        nodes = {}
        self.arc = []
        if self.groundnet == None:
            return
        nodes_xml = self.groundnet.find("TaxiNodes")
        txiway_xml = self.groundnet.find("TaxiWaySegments")
        if nodes_xml is not None:
            for node in nodes_xml.findall("node"):
                lat = degtofloat(node.attrib["lat"])
                lon = degtofloat(node.attrib["lon"])
                nodes[node.attrib["index"]] = [
                        lon,
                        lat
                ]
        if txiway_xml is not None:
            for taxiway in txiway_xml.findall("arc"):
                try:
                    self.arc.append({
                            'n0': nodes[taxiway.attrib["begin"]],
                            'n1': nodes[taxiway.attrib["end"]]
                        })
                except KeyError:
                    pass  # not this time :(

    def _process_coordinate_for_boundary(self, lon, lat):
        if(lat < self.min_lat):
            self.min_lat = lat
        if(lat > self.max_lat):
            self.max_lat = lat
        if(lon < self.min_lon):
            self.min_lon = lon
        if(lon > self.max_lon):
            self.max_lon = lon
    def calculate_airport_map_boundaries(self):
        self.min_lon = 180
        self.max_lon = -180
        self.min_lat = 90
        self.max_lat = -90

        if self.threshold is not None:
            for rwy in self.threshold.findall("runway"):
                for th in rwy.findall("threshold"):
                    lon = float(th.find("lon").text)
                    lat = float(th.find("lat").text)
                    self._process_coordinate_for_boundary(lon, lat)

        if self.twr is not None:
            for tower in self.twr.findall("tower"):
                for twr in tower.findall("twr"):
                    lon = float(twr.find("lon").text)
                    lat = float(twr.find("lat").text)
                    self._process_coordinate_for_boundary(lon, lat)

        if self.groundnet is not None:
            if self.groundnet.find("parkingList") != None:
                parkings = self.groundnet.find("parkingList")
                for parking in parkings.findall("Parking"):
                    lon = float(degtofloat(parking.attrib["lon"]))
                    lat = float(degtofloat(parking.attrib["lat"]))
                    self._process_coordinate_for_boundary(lon, lat)

        R = 6371000
        self.min_x = R * self.min_lon/180*pi
        self.max_x = R * self.max_lon/180*pi
        self.min_y = R * mercY(self.min_lat)
        self.max_y = R * mercY(self.max_lat)
        self.dx = self.max_x - self.min_x
        self.dy = self.max_y - self.min_y

        self.landscape = self.dx > self.dy

        self.map_width = ap_map_long if self.landscape else ap_map_short
        self.map_height = (
            ap_map_long if not self.landscape else ap_map_short)-ap_map_top_margin

    def make_ground_chart(self):
        properties = {
            'landscape': self.landscape,
            'margin': ap_map_margin,
        }
        with open(self.target + "/ground_chart.tex", "w") as tex:
            tex.write(env.get_template('ground_chart.tex.j2')
                      .render(properties))

        scaled_length = 100*self.map_width/self.dx
        l = scaled_length
        length = 100
        while l < .7:
            l += scaled_length
            length += 100
        scaled_length = l
        scale_factor = 4
        properties = {
            'to_tikz_coord': self.to_tikz_coord,
            'date': datetime.datetime.now().strftime("%d %b %y").lower(),
            'map_width': self.map_width,
            'map_height': self.map_height,
            'icao': self.icao,
            'scaled_length': scaled_length,
            'length': length,
            'scale_factor': scale_factor,
            'tick_size': 0.2,
            'max_scale': 4,
            'runways': [] if self.threshold is None else map(Runway, self.threshold.findall("runway")),
            'towers': [] if self.twr is None else [Tower(twr) for tower in self.twr.findall("tower") for twr in tower.findall("twr")],
        }

        with open(self.target + "/ground_chart.tikz", "w") as tikz:
            tikz.write(env.get_template('ground_chart.tikz.tex.j2')
                      .render(properties))

    def to_tikz_coord(self, coords):
        extra_margin = 3
        ymin = mercY(self.min_lat)
        ymax = mercY(self.max_lat)
        xscale = (self.map_width-extra_margin) / \
            ((self.max_lon-self.min_lon)/180*pi)
        yscale = (self.map_height-extra_margin)/(ymax-ymin)
        if(yscale > xscale):
            yscale = xscale
        else:
            xscale = yscale
        x = coords[0]/180*pi
        y = mercY(coords[1])
        x = extra_margin/2 + (x - self.min_lon/180*pi)*xscale\
            + (self.map_width - extra_margin -
               (self.max_lon-self.min_lon)*xscale/180*pi)/2
        y = extra_margin/2 + (y - ymin)*yscale \
            + (self.map_height - extra_margin - (ymax-ymin)*yscale)/2
        return (x, y)

    def compile(self):
        mainfile = self.icao + ".tex"
        with open(os.devnull, "w") as fnull:
            call(["latexmk", "-shell-escape", "-pdf", mainfile],
                 stdout=fnull, stderr=fnull, cwd=self.target)

class Runway:

    def __init__(self, el):
        # TODO: I don't believe this can't be cleaner.
        self.coords = []
        self.rwys = []
        self.heading = []
        for th in el.findall("threshold"):
            lon = float(th.find("lon").text)
            lat = float(th.find("lat").text)
            self.coords.append((lon, lat))
            self.rwys.append(th.find("rwy").text)
            self.heading.append(float(th.find("hdg-deg").text))
        self.theta = (90-self.heading[0])/180 * pi
        self.d = .5  # centimeters
        h = atan2(mercY(self.coords[1][1])-mercY(self.coords[0][1]),
                (self.coords[1][0]-self.coords[0][0])/180*pi)*180/pi - 90
        if(h > 0):
            h = h % 90
        else:
            h = - ((-h) % 90)
        while h > 45:
            h = h-90
        while h < -45:
            h = h+90
        self.h = h

class Tower:

    def __init__(self, el):
        self.lon = float(el.find('lon').text)
        self.lat = float(el.find('lat').text)
        self.coords = (self.lon, self.lat)
