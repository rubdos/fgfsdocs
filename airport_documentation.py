#!/usr/bin/env python3
import requests
import datetime
import sys
import os

from airport import Airport

# TODO: implement taxiways


def print_usage():
    print("Usage:")
    print(sys.argv[0] + " ICAO")
    sys.exit(0)


with requests.Session() as session:
    wd = os.getcwd()
    i = 0
    if sys.argv[1] == "all":
        print("Downloading all airports; might take a while")
        airports = Airport.get_all(session)
    else:
        airports = [Airport(sys.argv[1].upper())]

    for airport in airports:
        ICAO = airport.icao
        i += 1
        print(i)
        root = "build/charts/" + ICAO
        airport.fetch_files(session)
        try:
            airport.calculate_airport_map_boundaries()
        except ValueError as e:
            print("%s has problems with mercY" % airport.icao)
            continue
        airport.prepare_taxiways()
        airport.build_chart(root)
        try:
            airport.make_ground_chart()
        except ZeroDivisionError as e:
            print("%s has problems with dx" % airport.icao)
        airport.compile()

        # if sys.argv[1] != 'all':
        #    call(["xdg-open", ICAO+".pdf"])
