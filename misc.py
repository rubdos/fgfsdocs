import os
from math import cos, sin, pi, atan2, tan, log


def ensure_dir(path):
    if not os.path.isdir(path):
        os.makedirs(path)


def mercY(lat):
    lat = lat/180*pi
    return log(tan(lat/2 + pi/4))


def degtofloat(deg):
    c = deg[0]
    sign = 1
    if c == "S" or c == "W":
        sign = -1
    idx = deg.index(' ')
    f = float(deg[1:idx])
    f += float(deg[idx+1:])/60
    return sign*f

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step
